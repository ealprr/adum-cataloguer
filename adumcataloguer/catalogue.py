# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup


class FormationItem():
    status = None
    name = None
    url = None
    ville = None
    responsable = None
    date = None

    def __init__(self, status=None, name=None, url=None, ville=None, responsable=None, date=None):
        self.status = status
        self.name = name
        self.url = url
        self.ville = ville
        self.responsable = responsable
        self.date = date


class Catalogue():

    ENCODING = 'utf-8'

    def __init__(self, url=None):
        if not url:
            self.url = 'http://www.adum.fr/script/actions.pl?&site=UDG'
        else:
            self.url = url

    def crawl_all_pages(self):
        html_str = self.crawl(self.url)
        soup = BeautifulSoup(html_str)
        b_items = soup.find(id="g").find_all('b')

        next_urls = []
        for b_item in b_items:
            if b_item.text.find(u'Nombre de réponses') == 0:
                a_items = b_item.find_all('a')
                for a_item in a_items:
                    if a_item['href'].find('http://www.adum.fr/script/actions.pl?lieu=&cat') == 0:
                        next_urls.append(a_item['href'])
                    else:
                        break
            if len(next_urls) > 0:
                break

        formations = []
        formations.extend(self.parse_response(html_str))

        for url in next_urls:
            formations.extend(self.parse_response(self.crawl(url)))

        return formations

    def crawl(self, url):
        r = requests.get(url)
        html_str = r.text.encode('iso-8859-1')
        return html_str

    def parse_response(self, html_doc):
        formations = []
        soup = BeautifulSoup(html_doc)
        tr_items = soup.find_all('table')[-1].find_all('tr')

        for tr_item in tr_items:
            formation = self.parse_tr(tr_item)
            if formation:
                formations.append(formation)
        return formations

    def parse_tr(self, html_tr):
        if len(html_tr.find_all('td')) != 6:
            return None

        formation = FormationItem()
        td_items = html_tr.find_all('td')

        formation.status = td_items[0].text.replace(u'\xa0', '')
        formation.name = td_items[2].text.replace(u'\xa0', '').replace(u'', "'")
        formation.url = td_items[2].a['href']
        formation.ville = td_items[3].text.replace(u'\xa0', '')
        formation.responsable = td_items[4].text.replace(u'\xa0', '')
        formation.date = td_items[5].text.replace(u'\xa0', '')

        if formation.status == u'Etat' and formation.name == u'Libellé' and formation.date == u'Début' and \
            formation.responsable == u'Responsable' and formation.ville == u'Ville':
            return None
        return formation

    def get_formations(self):
        return self.crawl_all_pages()
