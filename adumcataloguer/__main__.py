#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
adumcataloguer.main
-----------------------------------

Main entry point for the `adum-cataloguer` command.

"""

import argparse
import catalogue
import sys


def get_adumcataloguer_args():
    """
    Get the command line input/output arguments passed into ADUM Cataloguer.
    """

    parser = argparse.ArgumentParser(
        description='ADUM Cataloguer is a tool to fetch the course list from adum.fr.'
    )

    # TODO: parser.add_argument(...)

    args = parser.parse_args()
    return args


def main():
    args = get_adumcataloguer_args()
    
    # TODO: call function that does the main work here
    adum_catalogue = catalogue.Catalogue()

    formations = adum_catalogue.get_formations()
    
    for formation in formations:
        # print("[%s] %s (%s) - %s - %s - %s" % (formation.status, formation.name, formation.url, formation.ville,
        #                                        formation.responsable, formation.date))
        formation_str = u"* [%s] **[%s](%s)** - %s - %s - %s\n" % (formation.status, formation.name, formation.url, formation.ville, 
                                                            formation.responsable, formation.date)
        #print(formation_str.encode('utf-8'))
        sys.stdout.write(formation_str.encode('utf-8'))


if __name__ == '__main__':
    main()
