#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ADUM Cataloguer
---------------

ADUM Cataloguer is a tool to fetch the course list from adum.fr.
"""

from __future__ import absolute_import, division, print_function, \
    with_statement, unicode_literals

#from .adumcataloguer import Adumcataloguer
from .catalogue import Catalogue