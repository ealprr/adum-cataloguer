__all__ = [
    "__title__", "__package_name__", "__description__", "__url__", "__version__",
    "__author__", "__email__", "__license__", "__copyright__",
]

__title__ = 'adum-cataloguer'
__package_name__ = 'adumcataloguer'
__description__ = 'ADUM Cataloguer is a tool to fetch the course list from adum.fr.'
__url__ = 'https://github.com//adumcataloguer'
__version__ = '0.1.0'
__author__ = 'ealprr'
__email__ = 'ealprr@gmail.com'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2015 ealprr'
