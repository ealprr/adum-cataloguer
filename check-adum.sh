#!/bin/sh

# Define the path to your catalogue repository
CAT_REPO="catalogue"

if [ ! -d "$CAT_REPO" ]
then
	mkdir "$CAT_REPO"
	cd "$CAT_REPO"
	git init > /dev/null
	cd ..
fi

# Comment the following line if you do not use virtual environnement
. ./venv/bin/activate
python adumcataloguer/__main__.py > "$CAT_REPO/formations.txt"

if [ $? -eq 0 ]; then
	cd "$CAT_REPO"

	if [ -z "$(git status -s)" ]
	then
		echo 'no diff' > /dev/null
	else
		if [ "$(git status -s)" \> "??" ]
		then
			git add formations.txt > /dev/null
			git commit -m "$(date)" > /dev/null
			#echo ''
		else
			if [ "$(git status -s)" \> ' M' ]
			then
				git add formations.txt > /dev/null
				git commit -m "$(date)" > /dev/null
				git --no-pager diff HEAD^ HEAD -U0
			else
				git status -s
			fi
		fi
	fi
fi

# Comment the following line if you do not use virtual environnement
deactivate
