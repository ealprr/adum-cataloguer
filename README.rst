===============
ADUM Cataloguer
===============

ADUM Cataloguer is a tool to fetch the course list from adum_ website.

.. _adum: https://www.adum.fr

-----
Usage
-----

To use ADUM Cataloguer to fetch the course list and print it::
    $ adum-cataloguer

You can also use the shell script in order to version changes in a git repository::
    ./check-adum.sh

To use ADUM Cataloguer in a project::

    import adumcataloguer

------------
Installation
------------

At the command line::

    $ easy_install adum-cataloguer

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv adum-cataloguer
    $ pip install adum-cataloguer
