#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

about = {}
with open("adumcataloguer/__about__.py") as fp:
    exec(fp.read(), about)

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.rst') as changelog_file:
    changelog = changelog_file.read().replace('.. :changelog:', '')

with open('requirements.txt') as f:
    requirements = [line for line in f.read().split('\n') if line]
    test_requirements = []

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme + '\n\n' + changelog,
    author=about['__author__'],
    author_email=about['__email__'],
    url=about['__url__'],
    packages=find_packages(exclude=['docs']),
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    license=about['__license__'],
    keywords=about['__title__'],
    classifiers=[ # http://pypi.python.org/pypi?:action=list_classifiers
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    entry_points={
        'console_scripts': [
            'adum-cataloguer = adumcataloguer.__main__:main'
        ]
    },
)
